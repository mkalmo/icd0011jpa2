package ex;

import common.LoggingListener;
import ex.config.DatabaseConfig;
import ex.model.Ticket;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

public class TicketRepositoryTests {

    private TicketRepository repository;

    private TransactionTemplate transactionTemplate;

    @Test
    public void loadTicket() {
        Ticket ticket = repository.findTicketById(1L);

        System.out.println(ticket);
    }

    @Test
    public void loadTicket1() {
        transactionTemplate.executeWithoutResult(status -> {
            Ticket ticket = repository.findTicketById(1L);

            System.out.println(1);

            System.out.println(ticket);
        });
    }



    @Before
    public void setUp() {
        LoggingListener.setLoggingEnabled(true);

        ConfigurableApplicationContext ctx =
                new AnnotationConfigApplicationContext(DatabaseConfig.class);

        repository = ctx.getBean(TicketRepository.class);

        transactionTemplate = new TransactionTemplate(ctx.getBean(PlatformTransactionManager.class));
    }

}
