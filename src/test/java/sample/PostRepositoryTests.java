package sample;

import common.LoggingListener;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import sample.config.DatabaseConfig;
import sample.model.*;

public class PostRepositoryTests {

    private PostRepository postRepository;

    private TransactionTemplate transactionTemplate;

    @Test
    public void addReplyToPost() {
        Post post = postRepository.findPostById(1L);

        System.out.println(post);

        post.getReplies().add(new Reply("Added reply"));

        postRepository.save(post);
    }

    @Test
    public void addReplyToPostInTransaction() {
        transactionTemplate.execute(status -> {
            Post post = postRepository.findPostById(1L);

            System.out.println(post);

            post.getReplies().add(new Reply("Added reply"));

            return postRepository.save(post);
        });
    }

    @Before
    public void setUp() {
        LoggingListener.setLoggingEnabled(true);

        ConfigurableApplicationContext ctx =
                new AnnotationConfigApplicationContext(DatabaseConfig.class);

        postRepository = ctx.getBean(PostRepository.class);

        transactionTemplate = new TransactionTemplate(ctx.getBean(PlatformTransactionManager.class));
    }

}
