package sample;

import common.LoggingListener;
import org.hibernate.LazyInitializationException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import sample.config.DatabaseConfig;
import sample.model.*;

public class PersonRepositoryTests {

    private PersonRepository personRepository;

    @Test(expected = LazyInitializationException.class)
    public void findAllPersonsFails() {
        var persons = personRepository.findAllPersons1();

        System.out.println(persons);
    }

    @Test
    public void findAllPersonsShallow() {
        System.out.println(personRepository.findAllPersons2());
        System.out.println(personRepository.findAllPersons3());
        System.out.println(personRepository.findAllPersons4());
    }

    @Test
    public void addPhoneToPerson() {
        LoggingListener.setLoggingEnabled(false);

        Person person = personRepository.findPersonById(1L);

        System.out.println(person);

        person.getPhones().add(new Phone("+372 789"));

        personRepository.save(person);

        System.out.println(personRepository.findPersonById(1L));
    }

    @Before
    public void setUp() {
        ConfigurableApplicationContext ctx =
                new AnnotationConfigApplicationContext(DatabaseConfig.class);

        personRepository = ctx.getBean(PersonRepository.class);
    }

}
