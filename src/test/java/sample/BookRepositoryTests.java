package sample;

import common.LoggingListener;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import sample.config.DatabaseConfig;
import sample.model.*;

public class BookRepositoryTests {

    private BookRepository repository;

    @Test
    public void findBookFull() {
        Book book = repository.findBookById(1L);

        System.out.println(book);
    }

    @Test
    public void findBookBad() {
        BookBad book = repository.findBookByIdBad(1L);

        System.out.println(book);
    }

    @Test
    public void findBookShallow() {
        BookModified book = repository.findBookByIdShallow(1L);

        System.out.println(book);
    }

    @Test
    public void saveBookSettingReference() {
        BookModified book = repository.findBookByIdShallow(1L);
        book.setAuthorId(2L);

        repository.save(book);

        System.out.println(repository.findBookById(1L));
    }

    @Before
    public void setUp() {
        LoggingListener.setLoggingEnabled(false);

        ConfigurableApplicationContext ctx =
                new AnnotationConfigApplicationContext(DatabaseConfig.class);

        repository = ctx.getBean(BookRepository.class);
    }

}
