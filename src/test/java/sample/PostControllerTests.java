package sample;

import common.LoggingListener;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import sample.config.MvcConfig;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {MvcConfig.class})
public class PostControllerTests {
    static {
        LoggingListener.setLoggingEnabled(true);
    }

    private WebApplicationContext ctx;

    @Autowired
    public void setCtx(WebApplicationContext ctx) {
        this.ctx = ctx;
    }

    @Test
    public void patchPostWithJsonPatch() {
        String requestBody = """
                [{ "op": "add", "path": "/replies/-", "value": "Reply 1_3" }]
                """;

        var response = sendPatchRequest("/post/1", requestBody);

        System.out.println(response);
    }

    @SneakyThrows
    private String sendPatchRequest(String path, String contents) {

        MockMvc mvc = MockMvcBuilders.webAppContextSetup(ctx).build();

        MockHttpServletRequestBuilder request = patch(path)
                .content(contents)
                .header("Content-type", "application/json-patch+json");

        MvcResult result = mvc.perform(request).andReturn();

        return result.getResponse().getContentAsString();
    }
}