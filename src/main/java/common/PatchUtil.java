package common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;

public class PatchUtil {

    public static <T> T applyPatch(JsonPatch patch, T target) {

        try {
            JsonNode patched = patch.apply(
                    new ObjectMapper().convertValue(target, JsonNode.class));

            JavaType javaType = TypeFactory.defaultInstance()
                    .constructSimpleType(target.getClass(), new JavaType[0]);

            return new ObjectMapper().treeToValue(patched, javaType);

        } catch (JsonPatchException | JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }


}
