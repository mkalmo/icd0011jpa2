package common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import net.ttddyy.dsproxy.listener.logging.AbstractQueryLoggingListener;
import net.ttddyy.dsproxy.listener.logging.DefaultJsonQueryLogEntryCreator;

import java.io.IOException;
import java.util.List;

public class LoggingListener extends AbstractQueryLoggingListener {

    private static boolean loggingEnabled = false;

    public LoggingListener() {
        this.loggingCondition = () -> true;
        setQueryLogEntryCreator(new DefaultJsonQueryLogEntryCreator());
    }

    public static void setLoggingEnabled(boolean isEnabled) {
        loggingEnabled = isEnabled;
    }

    @Override
    protected void writeLog(String message) {
        if (!loggingEnabled) {
            return;
        }

        try {
            var map = new ObjectMapper().readValue(message, Message.class);

            System.out.printf("%s %s\n", map.query.get(0), map.params.get(0));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class Message {
        private List<String> query;
        private List<List<String>> params;
    }
}
