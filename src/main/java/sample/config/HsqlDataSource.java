package sample.config;

import common.LoggingListener;
import net.ttddyy.dsproxy.support.ProxyDataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;

@PropertySource("classpath:/app.properties")
public class HsqlDataSource {

    @Bean
    public DataSource dataSource(Environment env) {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName("org.hsqldb.jdbcDriver");
        ds.setUrl(env.getProperty("db.url"));

        var p = new ResourceDatabasePopulator(
                new ClassPathResource("sample/schema.sql"),
                new ClassPathResource("sample/person-data.sql"),
                new ClassPathResource("sample/post-data.sql"),
                new ClassPathResource("sample/book-data.sql"));

        DatabasePopulatorUtils.execute(p, ds);

        return ProxyDataSourceBuilder
                .create(ds)
                .listener(new LoggingListener())
                .build();
    }

}