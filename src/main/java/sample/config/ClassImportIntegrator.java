package sample.config;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.spi.BootstrapContext;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.integrator.spi.Integrator;
import org.hibernate.service.spi.SessionFactoryServiceRegistry;

import java.util.List;

public class ClassImportIntegrator implements Integrator {
 
    private final List<Class<?>> classImportList;
 
    public ClassImportIntegrator(
            List<Class<?>> classImportList) {
        this.classImportList = classImportList;
    }
 
    @Override
    public void integrate(
            Metadata metadata,
            BootstrapContext bootstrapContext,
            SessionFactoryImplementor sessionFactory) {

        for(Class<?> classImport : classImportList) {
            metadata.getImports().put(
                classImport.getSimpleName(),
                classImport.getName()
            );
        }
    }
 
    @Override
    public void disintegrate(
            SessionFactoryImplementor sessionFactory,
            SessionFactoryServiceRegistry serviceRegistry) {}
}