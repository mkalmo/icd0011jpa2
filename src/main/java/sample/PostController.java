package sample;

import com.github.fge.jsonpatch.JsonPatch;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.transaction.annotation.Transactional;

import sample.model.Post;

import static common.PatchUtil.applyPatch;

@RestController
public class PostController {

    private final PostRepository repository;

    public PostController(PostRepository repository) {
        this.repository = repository;
    }

    @PatchMapping("post/{id}")
    @Transactional
    public Post patchPost(
            @PathVariable("id") Long id,
            @RequestBody JsonPatch patch) {

        Post person = repository.findPostById(id);

        Post patched = applyPatch(patch, person);

        return repository.save(patched);
    }
}