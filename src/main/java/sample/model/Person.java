package sample.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "person")
public class Person {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "name")
    private String name;

    @ElementCollection
    @CollectionTable(
            name = "phone",
            joinColumns=@JoinColumn(name = "person_id", referencedColumnName = "id"))
    private List<Phone> phones;

    public Person(Long id, String name) {
        this.id = id;
        this.name = name;
    }

}
