package sample.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "book")
public class BookBad {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    @Column(name = "author_id")
    private Long authorId;

}
