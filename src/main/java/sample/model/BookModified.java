package sample.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "book")
public class BookModified {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    @OneToOne(fetch = FetchType.LAZY)
    private Author author;

    @Transient
    private Long authorId;

}
