package sample.model;

import lombok.*;

import jakarta.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
public class Post {

    @Id
    @GeneratedValue
    private Long id;

    @NonNull
    private String title;

    @ElementCollection
    @JoinTable(
            name = "reply",
            joinColumns =
              @JoinColumn(name="post_id", referencedColumnName="id"))
    @OrderColumn(name = "idx")
    private List<Reply> replies;

    @Override
    public String toString() {
        return String.format("(%s, %s, %s)",
                id, title, replies);
    }
}
