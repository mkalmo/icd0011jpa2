package sample;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sample.model.Author;
import sample.model.Book;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import sample.model.BookBad;
import sample.model.BookModified;

@Repository
public class BookRepository {

    @PersistenceContext
    private EntityManager em;

    public Book findBookById(Long id) {
        TypedQuery<Book> query = em.createQuery(
                "select b from Book b " +
                        "left join fetch b.author where b.id = :id",
                Book.class);

        query.setParameter("id", id);

        return query.getSingleResult();
    }

    public BookModified findBookByIdShallow(Long id) {
        TypedQuery<BookModified> query = em.createQuery(
                "select c from BookModified c where c.id = :id", BookModified.class);

        query.setParameter("id", id);

        BookModified book = query.getSingleResult();

        book.setAuthorId(book.getAuthor() == null ? null : book.getAuthor().getId());
        book.setAuthor(null);

        return book;
    }

    public BookBad findBookByIdBad(Long id) {
        TypedQuery<BookBad> query = em.createQuery(
                "select c from BookBad c where c.id = :id", BookBad.class);

        query.setParameter("id", id);

        return query.getSingleResult();
    }

    @Transactional
    public BookModified save(BookModified book) {
        if (book.getAuthorId() != null) {
            book.setAuthor(em.getReference(Author.class, book.getAuthorId()));
        }

        em.merge(book);

        return book;
    }

}
