package sample;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sample.model.Post;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;

@Repository
public class PostRepository {

    @PersistenceContext
    private EntityManager em;

    public Post findPostById(Long id) {
        TypedQuery<Post> query = em.createQuery(
                "select p from Post p " +
                        "left join fetch p.replies where p.id = :id",
                Post.class);

        query.setParameter("id", id);

        return query.getSingleResult();
    }

    @Transactional
    public Post save(Post p) {
        em.merge(p);

        return p;
    }
}
