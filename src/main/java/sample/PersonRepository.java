package sample;

import org.springframework.stereotype.Repository;
import sample.model.Person;
import sample.model.PhoneViewModel;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import java.util.List;

@Repository
public class PersonRepository {

    @PersistenceContext
    private EntityManager em;

    public List<Person> findAllPersons1() {
        return em.createQuery(
                "select p from Person p", Person.class)
                .getResultList();
    }

    public List<Person> findAllPersons2() {
        var persons = em.createQuery(
                        "select p from Person p", Person.class)
                .getResultList();

        persons.forEach(o -> o.setPhones(null));

        return persons;
    }

    public List<Person> findAllPersons3() {
        var persons = em.createQuery(
                "select p from Person p", Person.class)
                .getResultList();

        return persons.stream()
                .map(p -> new Person(p.getId(), p.getName()))
                .toList();
    }

    public List<PhoneViewModel> findAllPersons4() {
        return em.createQuery(
                "select new PhoneViewModel(p.id, p.name) from Person p",
                PhoneViewModel.class).getResultList();
    }

    @Transactional
    public Person findPersonById(Long id) {
        TypedQuery<Person> query = em.createQuery(
                "select c from Person c " +
                        "left join fetch c.phones where c.id = :id",
                Person.class);

        query.setParameter("id", id);

        return query.getSingleResult();
    }

    @Transactional
    public Person save(Person p) {
        em.merge(p);

        return p;
    }



}
