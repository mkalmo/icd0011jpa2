package ex.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.persistence.*;

@Data
@Entity
@Table(name = "developer")
public class Developer {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

}
