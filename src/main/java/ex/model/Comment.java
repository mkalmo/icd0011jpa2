package ex.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import jakarta.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "comment")
public class Comment {

    @Id
    @GeneratedValue
    private Long id;

    private String text;

    @OneToOne(fetch = FetchType.LAZY)
    private Developer developer;

    @Override
    public String toString() {
        return "%s, %s".formatted(text, developer);
    }
}
