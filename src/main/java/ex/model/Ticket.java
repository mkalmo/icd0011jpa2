package ex.model;

import lombok.*;

import jakarta.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "ticket")
public class Ticket {

    @Id
    @GeneratedValue
    private Long id;

    private String text;

    @OneToMany
    @JoinColumn(name="ticket_id")
    @OrderColumn(name = "idx")
    private List<Comment> comments;

    @OneToOne(fetch = FetchType.LAZY)
    private Developer developer;

    @Override
    public String toString() {
        return "(%s, %s, %s, %s)".formatted(id, text, developer, comments);
    }
}
