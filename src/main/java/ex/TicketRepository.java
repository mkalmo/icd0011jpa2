package ex;

import ex.model.Ticket;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;

@Repository
public class TicketRepository {

    @PersistenceContext
    private EntityManager em;

    public Ticket findTicketById(Long id) {
        TypedQuery<Ticket> query = em.createQuery(
                "select t from Ticket t " +
                        " left join fetch t.comments c" +
                        " left join fetch c.developer" +
                        " left join fetch t.developer" +
                        " where t.id = :id",
                Ticket.class);

        query.setParameter("id", id);

        return query.getSingleResult();
    }

    @Transactional
    public Ticket save(Ticket p) {
        em.merge(p);

        return p;
    }
}
