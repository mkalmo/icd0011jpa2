INSERT INTO developer (id, name) VALUES (1, 'Alice');
INSERT INTO developer (id, name) VALUES (2, 'Bob');
INSERT INTO developer (id, name) VALUES (3, 'Carol');

INSERT INTO ticket (id, text, developer_id) VALUES (1, 'Null pointer at ...', 1);
INSERT INTO ticket (id, text, developer_id) VALUES (2, 'App crashes when ...', 2);

INSERT INTO comment(id, ticket_id, developer_id, text, idx) VALUES (1, 1, 2, 'Can''t reproduce', 0);
INSERT INTO comment(id, ticket_id, developer_id, text, idx) VALUES (2, 1, 3, 'Please try ...', 1);
INSERT INTO comment(id, ticket_id, developer_id, text, idx) VALUES (3, 1, 2, 'Ok, found it', 2);

