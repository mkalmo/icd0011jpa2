DROP SCHEMA PUBLIC CASCADE;

CREATE SEQUENCE hibernate_sequence AS INTEGER START WITH 10;

CREATE TABLE developer (
   id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('hibernate_sequence'),
   name VARCHAR(255)
);

CREATE TABLE ticket (
  id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('hibernate_sequence'),
  text VARCHAR(255) NOT NULL,
  developer_id BIGINT NOT NULL
      REFERENCES developer(id) ON DELETE RESTRICT
);

CREATE TABLE comment (
  id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('hibernate_sequence'),
  ticket_id BIGINT NOT NULL
      REFERENCES ticket(id) ON DELETE RESTRICT,
  developer_id BIGINT NOT NULL
      REFERENCES developer(id) ON DELETE RESTRICT,
  text VARCHAR(255) NOT NULL,
  idx INTEGER NOT NULL
);

