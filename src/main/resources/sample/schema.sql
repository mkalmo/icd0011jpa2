DROP SCHEMA PUBLIC CASCADE;

CREATE SEQUENCE hibernate_sequence AS INTEGER START WITH 10;

CREATE TABLE person (
    id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('hibernate_sequence'),
    name VARCHAR(255)
);

CREATE TABLE phone (
    number VARCHAR(255),
    person_id BIGINT,
    FOREIGN KEY (person_id)
        REFERENCES person(id) ON DELETE CASCADE
);

CREATE TABLE post (
    id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('hibernate_sequence'),
    title VARCHAR(255) NOT NULL
);

CREATE TABLE reply (
     post_id BIGINT NOT NULL
         REFERENCES post(id) ON DELETE RESTRICT,
     text VARCHAR(255) NOT NULL,
     idx INTEGER NOT NULL
);

CREATE TABLE book (
    id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('hibernate_sequence'),
    title VARCHAR(255),
    author_id BIGINT NULL
);

CREATE TABLE author (
    id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('hibernate_sequence'),
    first_name VARCHAR(255),
    last_name VARCHAR(255)
);

